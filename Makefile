UUID=noim@kayahr
INSTALL_PATH=~/.local/share/gnome-shell/extensions/$(UUID)
ZIP=$(UUID).zip

all: package

compile:

clean:
	rm $(UUID).zip

package: compile
	zip $(ZIP) *.js *.json README COPYING 

install: package
	mkdir -p $(INSTALL_PATH) && \
	unzip -o $(ZIP) -d $(INSTALL_PATH)

uninstall:
	rm $(INSTALL_PATH) -rf

.PHONY: all compile clean package install uninstall

