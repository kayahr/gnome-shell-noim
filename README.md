gnome-shell-noim
================

This is a small Gnome Shell extension which hides the IM status from the
user menu (The offline icon left of the user name, the status combo box and
the notifications combo box)
