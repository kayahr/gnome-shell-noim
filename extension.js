/*
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 * See COPYING for copying conditions.
 */

const Main = imports.ui.main;
const ExtensionSystem = imports.ui.extensionSystem;

function init()
{
    // Nothing to do here
}

/**
 * Returns the user menu.
 */
function getUserMenu()
{
    var panel = Main.panel;
    var statusArea = panel.statusArea || panel._statusArea;
    if (!statusArea) return null;
    return statusArea.userMenu;
}

/**
 * Checks if the quit button extension is installed. When it is installed
 * then we must not hide the status icon.
 */
function hasQuitButton()
{
    return ExtensionSystem.enabledExtensions.indexOf(
        "quitbutton@azathoth92.hotmail.it") != -1;
}

function enable()
{
    var userMenu = getUserMenu();
    if (!userMenu) return;
    
    if (userMenu._offlineIcon && !hasQuitButton()) 
        userMenu._offlineIcon.hide();
    if (userMenu._notificationsSwitch)
        userMenu._notificationsSwitch.actor.hide();
    if (userMenu._statusChooser && userMenu._statusChooser._combo)
        userMenu._statusChooser._combo.actor.hide();
}

function disable()
{
    var userMenu = getUserMenu();
    if (!userMenu) return;
    
    if (userMenu._offlineIcon) 
        userMenu._offlineIcon.show();
    if (userMenu._notificationsSwitch)
        userMenu._notificationsSwitch.actor.show();
    if (userMenu._statusChooser && userMenu._statusChooser._combo)
        userMenu._statusChooser._combo.actor.show();
}
